# ZGo Back End

The API server behind the [ZGo.cash](https://zgo.cash) app.

## Dependencies

- Zcash Full node
- MongoDB

## Configuration

The file `zgo.cfg` needs to be modified to put the correct parameters for implementation.
