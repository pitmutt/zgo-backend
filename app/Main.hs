{-# LANGUAGE OverloadedStrings #-}

module Main where

import Config
import Control.Concurrent (forkIO)
import Database.MongoDB
import Network.Wai.Handler.Warp (defaultSettings, setPort)
import Network.Wai.Handler.WarpTLS (runTLS, tlsSettings)
import Web.Scotty
import ZGoBackend

main :: IO ()
main = do
  putStrLn "Reading config..."
  loadedConfig <- loadZGoConfig "zgo.cfg"
  let myTlsSettings =
        if c_useTls loadedConfig
          then Just $
               tlsSettings (c_certificate loadedConfig) (c_key loadedConfig)
          else Nothing
  putStrLn "Starting Server..."
  pipe <- connect $ host (c_dbHost loadedConfig)
  j <-
    access
      pipe
      master
      (c_dbName loadedConfig)
      (auth (c_dbUser loadedConfig) (c_dbPassword loadedConfig))
  if j
    then putStrLn "Connected to MongoDB!"
    else fail "MongoDB connection failed!"
  _ <- forkIO (setInterval 60 (checkZcashPrices pipe (c_dbName loadedConfig)))
  _ <- forkIO (setInterval 75 (scanZcash loadedConfig pipe))
  _ <- forkIO (setInterval 90 (scanPayments loadedConfig pipe))
  _ <- forkIO (setInterval 60 (checkPayments pipe (c_dbName loadedConfig)))
  _ <- forkIO (setInterval 60 (expireOwners pipe (c_dbName loadedConfig)))
  _ <- forkIO (setInterval 60 (updateLogins pipe loadedConfig))
  let appRoutes = routes pipe loadedConfig
  case myTlsSettings of
    Nothing -> scotty (c_port loadedConfig) appRoutes
    Just tls -> do
      apiCore <- scottyApp appRoutes
      runTLS tls (setPort (c_port loadedConfig) defaultSettings) apiCore
