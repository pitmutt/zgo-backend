# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2022-10-08

### Changed

- Refactored the `api/owner` POST endpoint.
- Added logic to remove extra whitespace from viewing key strings

## [1.1.0] - 2022-10-01

### Added

- Feature to record a payment when detected on-chain
- Function to record a payment in Xero
- Tests for Xero account code
- Fields in `XeroToken` for Xero payment account code
- Support for the YWallet memo format
- API endpoint to request a Xero invoice
- API endpoint to generate ZGo order from external invoice
- Type `XeroInvResponse`
- Type `XeroInvoice`
- Type `XeroToken`
- Type `Xero`
- API endpoint to query Xero configuration
- Field `crmToken` for `Owner`
- Field `externalInvoice` and `shortCode` for `Order`

### Changed

- Improved error handling for API server
- CoinGecko price feed to include New Zealand dollar (NZD)

## [1.0.0] - 2022-07-27

### Added

- New functionality to read transactions for the given viewing keys
- New functionality to mark orders as paid once payment is found on-chain
- New `Config` type to house the configuration parameters
- New field in `Owner` type to store toggle for payment confirmation
- New field in `Owner` type to store viewing key

### Changed

- Added chronological sorting to list of orders
- Added logic in `/api/owner` endpoint to validate viewing key before saving
- Updated tests for `/api/owner` to account for invalid viewing keys
- Added alphabetic sorting to list of items
- Refactored code to use new `Config` type
- Enhance `decodeHexText` to support Unicode
- Enhance `encodeHexText` to support Unicode
- Update tests for encode/decode of memos

### Fixed

- Fixed the PIN generation
- Fixed calculation of order total to ensure 8 decimal places
- Fixed test for looking for an order with incorrect ID
- Fixed payment scan to focus only on new transactions

## [0.1.0.2] - 2022-05-25

### Added

- Changelog
- `paid` field in ZGoOrder type
- Test for `api/order/:id` endpoint with an invalid ID

### Fixed

- Bug #1: crash when invalid ID was provided to `api/order/:id`

## [0.1.0.1] - 2022-05-20

### Added

- Parametrized fullnode credentials

## [0.1.0.0] - 2022-05-19

### Added

- BOSL license
- API end points
  - Country
  - Block
  - Node address
  - User
  - Owner
  - Order
  - Item
- Processing
  - Zcash transactions to ZGo items
  - Convert login memos to Users
  - Mark Owners as paid when payment is found on chain
  - Mark Owners as expired when expiration date is reached
