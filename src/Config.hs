{-# LANGUAGE OverloadedStrings #-}

module Config where

import qualified Data.ByteString as BS
import Data.Configurator
import Data.SecureMem
import qualified Data.Text as T

data Config =
  Config
    { c_dbHost :: String
    , c_dbName :: T.Text
    , c_dbUser :: T.Text
    , c_dbPassword :: T.Text
    , c_passkey :: SecureMem
    , c_nodeAddress :: T.Text
    , c_nodeUser :: BS.ByteString
    , c_nodePwd :: BS.ByteString
    , c_port :: Int
    , c_useTls :: Bool
    , c_certificate :: String
    , c_key :: String
    }
  deriving (Eq, Show)

loadZGoConfig :: Worth FilePath -> IO Config
loadZGoConfig path = do
  config <- load [path]
  dbHost <- require config "dbHost"
  dbName <- require config "dbName"
  dbUser <- require config "dbUser"
  dbPassword <- require config "dbPassword"
  nodeAddress <- require config "nodeAddress"
  nodeUser <- require config "nodeUser"
  nodePwd <- require config "nodePassword"
  passkey <- secureMemFromByteString <$> require config "passkey"
  port <- require config "port"
  useTls <- require config "tls"
  cert <- require config "certificate"
  key <- require config "key"
  return $
    Config
      dbHost
      dbName
      dbUser
      dbPassword
      passkey
      nodeAddress
      nodeUser
      nodePwd
      port
      useTls
      cert
      key
