{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}

module ZGoTx where

import Data.Aeson
import qualified Data.Bson as B
import qualified Data.Text as T
import Database.MongoDB
import GHC.Generics

-- | Type to model a ZGo transaction
data ZGoTx =
  ZGoTx
    { _id :: Maybe ObjectId
    , address :: T.Text
    , session :: T.Text
    , confirmations :: Integer
    , blocktime :: Integer
    , amount :: Double
    , txid :: T.Text
    , memo :: T.Text
    }
  deriving (Eq, Show, Generic)

parseZGoTxBson :: B.Document -> Maybe ZGoTx
parseZGoTxBson d = do
  i <- B.lookup "_id" d
  a <- B.lookup "address" d
  s <- B.lookup "session" d
  c <- B.lookup "confirmations" d
  am <- B.lookup "amount" d
  t <- B.lookup "txid" d
  m <- B.lookup "memo" d
  bt <- B.lookup "blocktime" d
  pure $ ZGoTx i a s c bt am t m

encodeZGoTxBson :: ZGoTx -> B.Document
encodeZGoTxBson (ZGoTx i a s c bt am t m) =
  if not (null i)
    then [ "_id" =: i
         , "address" =: a
         , "session" =: s
         , "confirmations" =: c
         , "blocktime" =: bt
         , "amount" =: am
         , "txid" =: t
         , "memo" =: m
         ]
    else [ "address" =: a
         , "session" =: s
         , "confirmations" =: c
         , "blocktime" =: bt
         , "amount" =: am
         , "txid" =: t
         , "memo" =: m
         ]

instance Val ZGoTx where
  cast' (Doc d) = do
    i <- B.lookup "_id" d
    a <- B.lookup "address" d
    s <- B.lookup "session" d
    c <- B.lookup "confirmations" d
    am <- B.lookup "amount" d
    t <- B.lookup "txid" d
    m <- B.lookup "memo" d
    bt <- B.lookup "blocktime" d
    Just (ZGoTx i a s c bt am t m)
  cast' _ = Nothing
  val (ZGoTx i a s c bt am t m) =
    case i of
      Just oid ->
        Doc
          [ "_id" =: i
          , "address" =: a
          , "session" =: s
          , "confirmations" =: c
          , "blocktime" =: bt
          , "amount" =: am
          , "txid" =: t
          , "memo" =: m
          ]
      Nothing ->
        Doc
          [ "address" =: a
          , "session" =: s
          , "confirmations" =: c
          , "blocktime" =: bt
          , "amount" =: am
          , "txid" =: t
          , "memo" =: m
          ]
